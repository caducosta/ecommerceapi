const mongoose = require('mongoose');
// const CupomDesconto = require('./app/models/cupomdesconto');
const cupomDescontoCon = require('./app/controllers/cupomdesconto');
const produtoCon = require('./app/controllers/produto');

module.exports = function(ecommerceRouter){
  // criar rotas para o router
  // rotas que terminem em /cupons => servira para post e get
  ecommerceRouter.route('/cupons')    
    .post(cupomDescontoCon.adicionar)//cadastrar cupom    
    .get(cupomDescontoCon.listarTudo)//recuperar todos cupons

  /**
   * servira para todas rotas que terminem /cupons/:cupons_id => get, put, patch, delete
   */
  ecommerceRouter.route('/cupons/:cupons_id')
    .get(cupomDescontoCon.listarUm)
    .put(cupomDescontoCon.alterar)    
    .patch(cupomDescontoCon.alterarParcial) //alteracao parcial de um objeto
    .delete(cupomDescontoCon.excluir)

  ecommerceRouter.route('/produtos')
    .post(produtoCon.adicionar)
    .get(produtoCon.listar)

  ecommerceRouter.route('/produtos/:produtos_id')
    .get(produtoCon.buscaPorCodigo)
    .put(produtoCon.atualizar)    
    .patch(produtoCon.atualizarParcial) //alteracao parcial de um objeto
    .delete(produtoCon.remover)
    
  return ecommerceRouter;
};