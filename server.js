const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyparser = require('body-parser');
const ecommerceRouter =  express.Router();
const hateoasLink =  require('express-hateoas-links');

const routes = require('./routes');

const Usuario = require('./app/models/usuario');
const Produto = require('./app/models/produto');
const ObjectId = mongoose.Types.ObjectId;

// const urlBanco = 'mongodb+srv://admin:senhabanco@cluster0-p4f4k.mongodb.net/ecommerce?retryWrites=true&w=majority';

const porta = process.env.PORT || 3000;
// mongoose.connect("mongodb+srv://admin:senhabanco@cluster0-p4f4k.mongodb.net/ecommerce?retryWrites=true&w=majority", {useNewUrlParser:true});
mongoose.connect("mongodb://192.168.56.101:27017/ecommerce", {useNewUrlParser:true});


app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use(hateoasLink);

app.use((req, res, next) => {  
  next();
})

app.get('/', (req, res) => {
  res.send('bem vindo a loja virtual');
});

app.get('/usuarios', (req, res) => {  
  //get usuarios cadastrados no mongoDB
  Usuario.find((erro, usuarios) => {
    if(erro){      
      res.send('Erro ao recuperar usuários', erro);
    }
    else{
      res.json(usuarios);
    } 
  });
});


// ecommerceRouter.route('/produtos')
//   .post()
//   .get();

// ecommerceRouter.route('/produtos/:produtos_id')
//   .get()
//   .put()
//   .patch()
//   .delete()

app.use('/ecommerce', routes(ecommerceRouter));

app.listen(porta, (req, res) => {
	console.log(`servidor inicializado na porta ${porta}`);
})