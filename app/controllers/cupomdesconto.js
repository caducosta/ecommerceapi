const mongoose = require('mongoose');
const CupomDesconto = require('../models/cupomdesconto');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  listarTudo: (req, res) => {    
    CupomDesconto.find((erro, cupons) => {
      if(erro){
        res.status(502).json({message:'Erro ao recuperar usuários', erro});
      }
      else{
        res.statusCode = 200;
        res.json(cupons);
      } 
    });
  },
  
  adicionar: (req, res) => {
    let cupomDesconto
    try {
       cupomDesconto = new CupomDesconto(req.body);
    } catch (error) {
      res.status(400).json(error);
      return;
    }

    //Validacao Manual - forçar validacao 

    const error = cupomDesconto.validateSync();
    if(error){
      console.log('Mongoose Validation - Erro ao salvar Cupom de Desconto');
      res.status(400).json(error);
      return;
    }

    cupomDesconto.save((erro,novoCupom) => {
      if(erro){        
        res.status(500).json({message: 'erro ao gravar cupom', erro});
      }

      let url = req.protocol + '://' + req.get('host') + req.originalUrl;


      res.status(201).json(novoCupom,
        [
          {rel: 'recuperar', href: url + novoCupom._id, method: 'GET'},
          {rel: 'deletar', href: url + novoCupom._id, method: 'DELETE', title: 'Excluir cupom de desconto'}
        ]
      );
    })
  },

  listarUm: (req, res) => {  
    if(ObjectId.isValid(req.params.cupons_id)){
      res.status(400).json({
        message: 'Código inválido'
      });
      return;
    }
    CupomDesconto.findById(ObjectId(req.params.cupons_id), (error, cupomDesconto) => {
      if(error){
        res.status(502).json({message: 'erro ao recuperar cupom de desconto' + error});
      }else if(cupomDesconto){
        console.log(req)
        let url = req.protocol + '://' + req.get('host') + req.originalUrl;

        res.status(200).json(cupomDesconto,
          [
            {rel: 'alterar', href: url, method: 'PUT'},
            {rel: 'deletar', href: url, method: 'DELETE', title: 'Excluir cupom de desconto'}
          ]
        );
      }else{
        res.status(202).json(
          {
            message: "id do cupom não localizado",
            id: req.params.cupons_id
          }
        )
      }
    })
  },

  alterar: (req, res) => {
    CupomDesconto.findById(ObjectId(req.params.cupons_id), (error, cupomDesconto) => {
      if(error){
        res.status(500).json({message: 'erro ao recuperar cupom de desconto' + error});
      }else if(cupomDesconto){        
        
        if(req.body.dataInicial){
          cupomDesconto.dataInicial = req.body.dataInicial;
        }
        
        if(req.body.dataFinal){
          cupomDesconto.dataFinal = req.body.dataFinal;  
        }
        
        if(req.body.valorInicial){
          cupomDesconto.valorInicial = req.body.valorInicial;  
        }
        
        if(req.body.valorFinal){
          cupomDesconto.valorFinal = req.body.valorFinal;  
        }
        
        if(req.body.quantidadeCupons){
          cupomDesconto.quantidadeCupons = req.body.quantidadeCupons;  
        }
        
        if(req.body.quantidadeUsada){
          cupomDesconto.quantidadeUsada = req.body.quantidadeUsada;  
        }
        
        if(req.body.percentualDesconto){
          cupomDesconto.percentualDesconto = req.body.percentualDesconto;  
        }

        cupomDesconto.save(erro => {
          if(erro){
            res.status(500).json({message: 'erro ao gravar o cupom' + erro});
          }
          res.json({message: 'Cupom atualizado com sucesso'});
        })

      }else{
        res.status(404).json(
          {
            message: "id do cupom não localizado",
            id: req.params.cupons_id
          }
        )
      }
    })
  },

  excluir:(req, res) => {
    let id = req.params.cupons_id;

    CupomDesconto.deleteOne({_id: ObjectId(id)}, (erro, resultado) => {
      if(erro){
        res.status(504).json({message: 'erro ao excluir' + erro});
      }else if(resultado.n === 0){        
        res.status(202).json({message: 'Cupom informado não existe', id: id});
      }else{
        res.status(200).json({message: 'Cupom excluido com sucesso', id: id});
      }
    })
  },

  alterarParcial:(req, res) => {
    let id = req.params.cupons_id;
    let cupomDesconto =  req.body;

    const options = {runValidators: true};
    CupomDesconto.updateOne(
      {_id: ObjectId(id)}, 
      {$set: cupomDesconto},options,
       erro => {
      if(erro){
        res.status(400).json({message: 'erro ao alterar cupom parcialmente'+erro});
      }else{
        res.status(200).json({message: 'Cupom desconto atualizado'});
      }
    });
  }
}