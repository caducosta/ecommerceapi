const mongoose = require('mongoose');
const Produto = require('../models/produto');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  adicionar : (req, res) => {
    let produto
    try {
      produto = new Produto(req.body);
    } catch (error) {
      res.status(400).json(error);
      return;
    }
   
    // produto.nome = req.body.nome,
    // produto.preco = req.body.preco,
    // produto.descricao = req.body.descricao,
    // produto.quantidadeEstoque = req.body.quantidadeEstoque

    const error = novoProduto.validateSync();
    if(error){
      console.log('Mongoose Validation - Erro ao salvar Produto');
      res.status(400).json(error);
      return;
    }

    produto.save((error, novoProduto) => {
      if(error){        
        return res.status(500).json({message: 'erro ao gravar produto', error});
      }
      res.status(201).json(novoProduto);
    })
  },
  listar: (req, res) => {    
    Produto.find((erro, cupons) => {
      if(erro){
        res.send('Erro ao recuperar produtos', erro);
      }
      else{
        res.json(cupons);
      } 
    });
  },
  buscaPorCodigo: (req, res) => {  
    Produto.findById(ObjectId(req.params.produtos_id), (error, produto) => {
      if(error){
        res.send('erro ao recuperar produto', error);
      }else if(produto){
        res.json(produto);
      }else{
        res.json(
          {
            message: "id do produto não localizado",
            id: req.params.produtos_id
          }
        )
      }
    })
  },
  atualizar: (req, res) => {    
    Produto.findById(ObjectId(req.params.produtos_id), (error, produto) => {
      if(error){
        res.send('erro ao recuperar produto', error);
      }else if(produto){
        produto.nome = req.body.nome,
        produto.preco = req.body.preco,
        produto.descricao = req.body.descricao,
        produto.quantidadeEstoque = req.body.quantidadeEstoque
        //atualiza as informacoes
        if(req.body.nome){
          produto.nome = req.body.nome;            
        }

        if(req.body.preco){
          produto.preco = req.body.preco;            
        }

        if(req.body.descricao){
          produto.descricao = req.body.descricao;            
        }

        if(req.body.quantidadeEstoque){
          produto.quantidadeEstoque = req.body.quantidadeEstoque;            
        }
        
        produto.save(erro => {
          if(erro){
            res.send('erro ao gravar o produto');
          }
          res.json({message: 'Produto atualizado com sucesso'});
        })

      }else{
        res.json(
          {
            message: "id do produto não localizado",
            id: req.params.produtos_id
          }
        )
      }
    })
  },
  atualizarParcial:(req, res) => {
    let id = req.params.produtos_id;
    let produto =  req.body;

    Produto.updateOne({_id: ObjectId(id)}, {$set: produto}, erro => {
      if(erro){
        res.send('erro ao alterar produto parcialmente', erro);
      }else{
        res.json({message: 'Produto atualizado parcialmente'});
      }
    });
  },
  remover: (req, res) => {
    let id = req.params.produtos_id;    

    Produto.deleteOne({_id: ObjectId(id)}, (erro, resultado) => {
      if(erro){
        res.send('erro ao excluir', erro);
      }else if(resultado.n === 0){
        console.log(resultado)
        res.json({message: 'Produto informado não existe', id: id});
      }else{
        res.json({message: 'Produto excluido com sucesso', id: id});
      }
    })
  }
}