const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schemaProduto = new Schema({
  nome: {
    type: String,
    required: true,
    minlength: [3, 'valor `{VALUE}` inválido. Deve ser maior que `{MINLENGTH}`'],
    maxlength: [60, 'valor `{VALUE}` inválido. Deve ser no máximo `{MAXLENGTH}`']
  },
  preco: {
    type: Number,
    min: [0, 'valor `{VALUE}` inválido. Deve ser maior que `{MIN}`'],
  }, 
  descricao: {
    type:String,
    maxlength:[200, 'valor `{VALUE}` inválido. Deve ser no máximo `{MAXLENGTH}`']
  },
  quantidadeEstoque: {
    type: Number,
    default: 0
  } 
},{strict:"throw"});

const Produto = mongoose.model('Produto', schemaProduto, 'produto');

module.exports = Produto;