const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const schemaUsuario = mongoose.Schema(
  {
    nome:{
      type: String,
      required:true,
      trim:true,
      unique:true
    },
    email:String,
    senha: {
      type: String,
      required:true,
      trim:true
    }
  }
);

schemaUsuario.pre('save', function(next){
  const usuario = this; 
  if (!usuario.isModified || !usuario.isNew){
    next();
  }else{
    bcrypt.hash(usuario.senha, process.env.BRCRYPT_SALT_ROUNDS,
      function(err,hash){
        if(err){
          console.log('Erro ao encriptar senha do usuário', usuario.nome);
          next({message:err.message});
        } else {
          usuario.senha = hash; 
          next();
        }
      });
  }
})

const Usuario = mongoose.model('Usuario', schemaUsuario, 'usuario');

module.exports = Usuario;