const mongoose = require('mongoose');

let validarDataMaior = dataInformada => {
  return new Date(dataInformada) > new Date();
}

const enumTipo = {
  values: ['DESCONTO','FRETEGRATIS', 'PROMOCIONAL'],
  message: 'Valor nao definido'
}

const Schema = mongoose.Schema;

let schemaCupom = new Schema({
  dataInicial:Date,
  dataFinal:Date,
  valorInicial: Number,
  valorFinal: Number,
  quantidadeCupons:Number,
  quantidadeUsada: Number,
  percentualDesconto: Number,
  tipo: String
}, {strict: "throw"});

//VALIDACOES 

schemaCupom.path('dataInicial').validate(validarDataMaior);
schemaCupom.path('dataFinal').validate(function(){
  return (new Date(this.dataFinal) > new Date(this.dataInicial));
}, 'Data informada `{VALUE}` para o campo `{PATH}` deve ser maior que a data inicial');
schemaCupom.path('quantidadeCupons').min(0, 'valor `{VALUE}` inválido. Deve ser maior que `{MIN}`').max(999);
schemaCupom.path('percentualDesconto').required(true);
schemaCupom.path('tipo').enum(enumTipo).trim(true);

//PRE-SAVE
schemaCupom.pre('save', function(next){
  if(!this.tipo){
    console.log('entrou no if.Não informou o tipo');
    this.tipo = enumTipo.values[0];
  }
  next();
});

const CupomDesconto = mongoose.model('CupomDesconto', schemaCupom, 'cupomdesconto');

module.exports = CupomDesconto;